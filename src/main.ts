import { runharvester as harvy } from "./room/creeps/basicharvester";
import { spawn } from "./room/spawnman";

module.exports.loop = function () {
    Object.values(Game.creeps).forEach((creep) => {
	if(creep.memory.role == 'harvester') {
	    harvy(creep)
	}
	
    })
    
    spawn()
}
