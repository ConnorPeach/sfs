import { evalweight } from "../util/weight";
var creepneed = [1];
export function spawn() {
    var mostneeded = 0
    var creepcount = new Array(1).fill(0);
    Object.values(Game.creeps).forEach((creep) => {
	if(creep.memory.role == 'harvester') {
	    creepcount[0]++
	}
	
    })
    var weight = evalweight(creepcount, creepneed)
    if(mostneeded == 0) {
	var namenum = 0
	var n
	while(1) {
            n = Game.spawns['Spawn1'].spawnCreep([WORK, CARRY, MOVE], 'starter' + namenum, {memory:{role:'harvester', working: false}});
            //console.log(n)
            if(n != ERR_NAME_EXISTS) break
            namenum++
            
	}
    }
    
}				      
