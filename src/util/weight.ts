export function evalweight(weights: number[], vals: number[]) {
    var min = Infinity
    var index = 0
    for (var i = 0; i < weights.length; i++) {
	if(weights[i] * vals[i] < min) {
	    index = i
	    
	    min = weights[i] * vals[i]
	}
    }
    return {index: index, min: min}
}

